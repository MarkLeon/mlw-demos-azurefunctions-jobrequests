﻿# MLW.Demos.Azure.Functions.JobRequests

## About

 This is sample code for an Azure Functions demo.

## Use Case

This demo simulates a Job Request system.

## Design

[Diagram](docs/JobRequestsDemo-draw.io.jpg)

## Running/Debugging

This demo can be run locally in Visual Studio. It does not use any Azure resources. You do not need an Azure subscription to run this demo.

## Release Notes

v0.1.0 - Initial commit.

v1.0.0 - FWDNUG demo.

