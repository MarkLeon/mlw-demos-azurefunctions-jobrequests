using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using MLW.Demos.Azure.Functions.JobRequests.Shared.Config;
using MLW.Demos.Azure.Functions.JobRequests.Shared.Entities;
using Newtonsoft.Json;

namespace MLW.Demos.Azure.Functions.JobRequests.Scheduler
{
    /// <summary>
    /// Timer Trigger Function
    /// </summary>
    public static class TimerTriggerFunction
    {
        /// <summary>
        /// Function Logger
        /// </summary>
        public static ILogger Logger;

        /// <summary>
        /// Function Context
        /// </summary>
        public static ExecutionContext Context;

        /// <summary>
        /// Job Scheduler Service
        /// </summary>
        /// <param name="myTimer"></param>
        /// <param name="log"></param>
        /// <param name="context"></param>
        [FunctionName("JobRequestScheduler")]
        public static async Task RunAsync(
            [TimerTrigger("%JobRequestSchedule%")]
            TimerInfo myTimer, 
            ILogger log,
            ExecutionContext context)
        {
            // start
            log.LogInformation($"{context.FunctionName}: C# Timer trigger function executed at: {DateTime.Now}");

            // persist
            Logger = log;
            Context = context;

            // load app settings
            FunctionConfigHelper.LoadConfig(context.FunctionAppDirectory);

            // create new job request
            var newJobRequest = CreateNewSampleJobRequest();

            // post request
            await HttpPublishJobRequestAsync(newJobRequest);

            // end
            log.LogInformation($"{context.FunctionName}: C# Timer trigger function completed at: {DateTime.Now}");
        }

        public static string GetRandomAction()
        {
            var actionList = new List<string>
            {
                "LogInformation",
                "Make me a sandwich",
                "Eat a sandwich",
                "Take a nap"
            };
            var rand = new Random(DateTime.Now.Millisecond);
            return actionList[rand.Next(0, actionList.Count - 1)];
        }

        /// <summary>
        /// Create New Job Request
        /// </summary>
        /// <returns></returns>
        public static JobRequest CreateNewSampleJobRequest()
        {
            Logger.LogInformation($"{Context.FunctionName}: Creating new job request...");

            return new JobRequest
            {
                JobName = "SampleJob",
                RequestedAction = GetRandomAction(),
                RequesterId = "JobSchedulerService"
            };
        }

        /// <summary>
        /// HTTP Post Job Request to Job Service URI
        /// </summary>
        /// <param name="jobRequest"></param>
        /// <returns></returns>
        public static async Task HttpPublishJobRequestAsync(JobRequest jobRequest)
        {
            var uri = GetJobRequestServiceUri();

            var encodedContent = EncodeJobRequest(jobRequest);

            await HttpPostAsync(uri, encodedContent);
        }

        /// <summary>
        /// Get Job Request Service URI from App Settings
        /// </summary>
        /// <returns></returns>
        public static string GetJobRequestServiceUri()
        {
            Logger.LogInformation($"{Context.FunctionName}: Getting Job Request Service URI...");

            var uri = FunctionConfigHelper.GetConfig()["JobRequestServiceURI"];

            Logger.LogDebug($"{Context.FunctionName}: JobRequestServiceURI = {uri}");

            return uri;
        }

        /// <summary>
        /// Encode Job Request to JSON String Content
        /// </summary>
        /// <param name="jobRequest"></param>
        /// <returns></returns>
        public static StringContent EncodeJobRequest(JobRequest jobRequest)
        {
            // encode job request to json 
            Logger.LogInformation($"{Context.FunctionName}: Encoding job request...");

            return new StringContent(
                JsonConvert.SerializeObject(jobRequest),
                Encoding.UTF8,
                "application/json");
        }

        /// <summary>
        /// HTTP Post Encoded Content to URI
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="encodedContent"></param>
        /// <returns></returns>
        public static async Task HttpPostAsync(string uri, StringContent encodedContent)
        {
            using (var httpClient = new HttpClient())
            {
                using (var responseMessage = await httpClient.PostAsync(uri, encodedContent))
                {
                    var responseContent = responseMessage.Content.ReadAsStringAsync();

                    if (responseMessage.IsSuccessStatusCode)
                        Logger.LogInformation($"Success Response: {responseContent}");
                    else
                        Logger.LogError($"Failure Response: {responseContent}");

                    responseMessage.EnsureSuccessStatusCode();
                }
            }
        }
    }
}
