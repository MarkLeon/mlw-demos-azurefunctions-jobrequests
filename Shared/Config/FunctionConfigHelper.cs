﻿using Microsoft.Extensions.Configuration;

namespace MLW.Demos.Azure.Functions.JobRequests.Shared.Config
{
    /// <summary>
    /// Function Config Helper
    /// Loads and caches config settings, and makes available for consumers.
    /// </summary>
    public static class FunctionConfigHelper
    {
        /// <summary>
        /// Cached config settings
        /// </summary>
        private static IConfigurationRoot _cachedConfig = null;

        /// <summary>
        /// Function app directory
        /// </summary>
        private static string _functionAppDirectory = "";

        /// <summary>
        /// Return cached config settings.
        /// Load settings if not already loaded.
        /// </summary>
        /// <returns></returns>
        public static IConfigurationRoot GetConfig()
        {
            if (_cachedConfig == null) LoadConfig(_functionAppDirectory);
            return _cachedConfig;
        }

        /// <summary>
        /// Load settings into cache.
        /// </summary>
        public static void LoadConfig(string functionAppDirectory)
        {
            _functionAppDirectory = functionAppDirectory;

            _cachedConfig = new ConfigurationBuilder()
                .SetBasePath(_functionAppDirectory)
                .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();
        }
    }
}
