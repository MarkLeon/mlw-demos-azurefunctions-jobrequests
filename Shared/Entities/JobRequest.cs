﻿using System;

namespace MLW.Demos.Azure.Functions.JobRequests.Shared.Entities
{
    /// <summary>
    /// Sample Job Request
    /// </summary>
    public class JobRequest
    {
        /// <summary>
        /// Job Name
        /// </summary>
        public string JobName { get; set; }

        /// <summary>
        /// Requested Action
        /// </summary>
        public string RequestedAction { get; set; }

        /// <summary>
        /// Requester ID
        /// </summary>
        public string RequesterId { get; set; }

        /// <summary>
        /// Job Request ID
        /// </summary>
        public string JobRequestID { get; set; } = Guid.NewGuid().ToString();

        /// <summary>
        /// Job Request Date/Time
        /// </summary>
        public DateTime JobRequestDateTime { get; set; } = DateTime.Now;
    }
}
