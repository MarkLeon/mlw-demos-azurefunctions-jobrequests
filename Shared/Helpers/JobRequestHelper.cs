﻿using System;
using MLW.Demos.Azure.Functions.JobRequests.Shared.Entities;

namespace MLW.Demos.Azure.Functions.JobRequests.Shared.Helpers
{
    public static class JobRequestHelper
    {
        public static bool IsValid(this JobRequest jobRequest)
        {
            if (string.IsNullOrEmpty(jobRequest.JobRequestID)) return false;
            if (string.IsNullOrEmpty(jobRequest.JobName)) return false;
            if (string.IsNullOrEmpty(jobRequest.RequestedAction)) return false;
            if (string.IsNullOrEmpty(jobRequest.RequesterId)) return false;

            return true;
        }

        public static void Validate(this JobRequest jobRequest)
        {
            if (!jobRequest.IsValid()) throw new Exception("JobRequest is invalid.");
        }
    }
}
