﻿using System.Threading.Tasks;
using MLW.Demos.Azure.Functions.JobRequests.Shared.Entities;

namespace MLW.Demos.Azure.Functions.JobRequests.Shared.Jobs
{
    /// <summary>
    /// Sample Job Runner Interface 
    /// </summary>
    /// <returns></returns>
    public interface IJob
    {
        /// <summary>
        /// Run job asynchronously 
        /// </summary>
        /// <returns></returns>
        Task RunAsync(JobRequest request);

        /// <summary>
        /// Run job synchronously 
        /// </summary>
        /// <returns></returns>
        void Run(JobRequest request);
    }
}
