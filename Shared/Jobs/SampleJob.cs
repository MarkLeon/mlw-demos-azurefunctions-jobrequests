﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MLW.Demos.Azure.Functions.JobRequests.Shared.Entities;

namespace MLW.Demos.Azure.Functions.JobRequests.Shared.Jobs
{
    /// <inheritdoc />
    /// <summary>
    /// Sample Job Runner Class 
    /// </summary>
    /// <returns></returns>
    public class SampleJob : IJob
    {
        /// <summary>
        /// Sample Job Logger
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor with injected logger
        /// </summary>
        /// <param name="logger"></param>
        public SampleJob(ILogger logger)
        {
            _logger = logger;
        }

        /// <inheritdoc />
        /// <summary>
        /// Run job asynchronously 
        /// </summary>
        /// <returns></returns>
        public async Task RunAsync(JobRequest request)
        {
            // perform job's unit of work here...

            // hacking synchronous operation to be async
            await Task.Run(() =>
            {
                // sample unit of work: process job request
                switch (request.RequestedAction)
                {
                    case "LogInformation":
                        // log informational message using logger

                        var logMessage = 
                            $"{GetType().Name}: Performing requested action...  \n" +
                            $"JobName = {request.JobName}  \n" + 
                            $"RequestedAction = {request.RequestedAction}  \n" + 
                            $"RequesterId = {request.RequesterId}  \n" +
                            $"JobRequestId = {request.JobRequestID}  \n" +
                            $"JobRequestDateTime = {request.JobRequestDateTime}  \n";

                        _logger.LogInformation(logMessage);

                        break;

                    default:
                        // log unsupported action request

                        var errorMessage =
                            $"{GetType().Name}: " +
                            $"This job does not support an action type of '{request.RequestedAction}'. " + 
                            $"[Job Request ID: {request.JobRequestID}]";

                        _logger.LogError(errorMessage);

                        break;
                }
            });
        }

        /// <inheritdoc />
        /// <summary>
        /// Run job synchronously 
        /// </summary>
        /// <returns></returns>
        public void Run(JobRequest request)
        {
            // create wrapper around async method
            Task.Run(async () =>
            {
                await RunAsync(request);

            }).GetAwaiter().GetResult();
        }
    }
}
