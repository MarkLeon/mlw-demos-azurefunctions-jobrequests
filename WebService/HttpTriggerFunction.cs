using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using MLW.Demos.Azure.Functions.JobRequests.Shared.Config;
using MLW.Demos.Azure.Functions.JobRequests.Shared.Entities;
using MLW.Demos.Azure.Functions.JobRequests.Shared.Helpers;
using MLW.Demos.Azure.Functions.JobRequests.Shared.Jobs;
using Newtonsoft.Json;

namespace MLW.Demos.Azure.Functions.JobRequests.WebService
{
    /// <summary>
    /// HTTP Trigger Function
    /// </summary>
    public static class HttpTriggerFunction
    {
        /// <summary>
        /// Function Logger
        /// </summary>
        public static ILogger Logger;

        /// <summary>
        /// Function Context
        /// </summary>
        public static ExecutionContext Context;

        /// <summary>
        /// Function Runner
        /// </summary>
        /// <param name="req"></param>
        /// <param name="log"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        [FunctionName("JobRequestService")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]
            HttpRequest req,
            ILogger log,
            ExecutionContext context)
        {
            // start
            log.LogInformation($"{context.FunctionName}: C# HTTP trigger function executed at: {DateTime.Now}");

            // persist function context vars
            Logger = log;
            Context = context;

            // load app settings
            FunctionConfigHelper.LoadConfig(context.FunctionAppDirectory);

            // process request
            ActionResult actionResult;

            try
            {
                // parse data from http request
                var jobRequest = await ParseNewJobRequestAsync(req);

                // validate input (ALWAYS!)
                jobRequest.Validate();

                // process data
                await ProcessJobRequestAsync(jobRequest);

                // acknowledge success
                var successMessage = $"{context.FunctionName}: Sample job request completed successfully. [{jobRequest.JobRequestID}]";
                log.LogInformation(successMessage);
                actionResult = new OkObjectResult(successMessage);
            }
            catch (Exception e)
            {
                // acknowledge error
                var errorMessage = $"{context.FunctionName}: An exception was encountered while attempting to run job: {e.Message}";
                log.LogError(errorMessage);
                actionResult = new BadRequestObjectResult(errorMessage);
            }

            // end
            log.LogInformation($"{context.FunctionName}: C# HTTP trigger function completed at: {DateTime.Now}");

            return actionResult;
        }

        /// <summary>
        /// Parse Job Request from HTTP Request
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public static async Task<JobRequest> ParseNewJobRequestAsync(HttpRequest req)
        {
            Logger.LogInformation($"{Context.FunctionName}: Parsing Job Request from HTTP Request...");

            // parse job request values using query string (GET)
            string jobName = req.Query["JobName"];
            string requestedAction = req.Query["RequestedAction"];
            string requesterId = req.Query["RequesterId"];

            // parse job request values using encoded body (POST)
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);

            jobName = jobName ?? data?.JobName;
            requestedAction = requestedAction ?? data?.RequestedAction;
            requesterId = requesterId ?? data?.RequesterId;

            // create new job request from input
            var jobRequest = new JobRequest
            {
                JobName = jobName,
                RequestedAction = requestedAction,
                RequesterId = requesterId
            };

            return jobRequest;
        }

        /// <summary>
        /// Process Job Request
        /// </summary>
        /// <param name="jobRequest"></param>
        /// <returns></returns>
        public static async Task ProcessJobRequestAsync(JobRequest jobRequest)
        {
            Logger.LogInformation($"{Context.FunctionName}: Processing Job Request...");

            switch (jobRequest.JobName)
            {
                case "SampleJob":
                    // run sample job
                    var job = new SampleJob(Logger);
                    await job.RunAsync(jobRequest);
                    break;

                default:
                    // unsupported job name
                    throw new Exception($"{Context.FunctionName}: JobName is unsupported: [{jobRequest.JobName}]");
            }
        }
    }
}
